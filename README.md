# Settings Tool

Laravel Nova Settings Tool

## About

Using this tool, you can easily create new custom fields with different types. Also, you can easily install multilingualism on this component. For an instant query to the database, all information is stored in the JSON format of groups.


### Prerequisites

This tool requires:

```
"laravel/nova": ">=2.8"
```

### Installing

Download via composer

```
composer require romarkcode/settings
```

Run migrations

```
php artisan migrate
```

Publish vendor files

```
php artisan vendor:publish --provider="Finoghentov\NovaSettings\ToolServiceProvider"
```

Add Tool to your NovaServiceProvider
```
    //NovaServiceProvider.php
    
    public function tools()
    {
        return [
            new NovaSettings
        ];
    }
```

Nova Settings constructor class expects two optional parameters.
- First one is `string` title in Nova sidebar.
- The second one is `boolean`, which can hide Configuration page

```
    //NovaServiceProvider.php
    
    public function tools()
    {
        return [
            new NovaSettings('Custom title', false)
        ];
    }
```

If you want to turn on multilingual , you should add follow code to your config/app.php
```
    //config/app.php
    
    'settings_languages' => [
      'en', ...
    ],
    
```

For using your settings you can use 'setting' helper method
```
   
   <div>
        {{setting('group_key.setting_key', 'locale')}}
   </div>
    
```

If you want to get array of group data you can use 'settings_group' helper method
```
foreach(settings_group('group_key') as $item){
    ...
}
```

To create and display new/existing settings in blade
```
   
   <div>
        {{setting_helper('group name', 'setting key', 'setting type', 'locale')}}
   </div>
    
```
'Setting type' it is enum field
```
default $type = 'text_box'
$type = [
    'text_box',
    'textarea',
    'rich_text_box',
    'checkbox',
    'image'
]
```
'Locale' by default it's NULL. If locale = null it will take app()->getLocale().
If setting doesn't have locale value, it returns app()->fallback_locale value.

### Cache settings
####Notice what we are using cache tags. Cache tags are not supported when using the file, dynamodb, or database cache drivers!
You can cache your query to db. You need to update config file romarkcode-settings.php
```
'cache_able'=true
```
You can change cache time if you need.Default it is 1 minute.You can put forever if you need
```
'cache_time' => 'forever'
```
If you need to clear cache you can clear by settings_group_key
```
cache()->tags($setting_group_key)->flush
```

![](/examples/example.gif)

