<?php

namespace Finoghentov\NovaSettings;

use Laravel\Nova\Nova;
use Laravel\Nova\Tool;

class NovaSettings extends Tool
{
    protected $title, $show_config;

    /**
     * nova-settings constructor.
     * @param string $title
     * @param bool $show_config
     * @param null $component
     */
    public function __construct($title = 'Nova Settings', $show_config = true, $component = null)
    {
        $this->title = $title;
        $this->show_config = $show_config;

        parent::__construct($component);
    }

    /**
     * Perform any tasks that need to happen when the tool is booted.
     *
     * @return void
     */
    public function boot()
    {
        Nova::script('nova-settings', __DIR__.'/../dist/js/tool.js');
        Nova::style('nova-settings', __DIR__.'/../dist/css/tool.css');
    }

    /**
     * Build the view that renders the navigation links for the tool.
     *
     * @return \Illuminate\View\View
     */
    public function renderNavigation()
    {   $title = $this->title;
        $show_config = $this->show_config;
        return view('nova-settings::navigation',compact('title', 'show_config'));
    }
}
