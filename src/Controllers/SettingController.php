<?php


namespace Finoghentov\NovaSettings\Controllers;


use Finoghentov\NovaSettings\Models\Setting;
use Finoghentov\NovaSettings\Requests\ApiRequest;
use Finoghentov\NovaSettings\Requests\Settings\AddGroupRequest;
use Finoghentov\NovaSettings\Requests\Settings\DeleteSettingRequest;
use Illuminate\Http\Request;


class SettingController extends ApiController
{
    /**
     * Return all settings at json format
     * @return Setting json
     */
    public function getSettings(){
        return parent::api_getSettings();
    }

    /**
     * @param AddGroupRequest $request
     * @return string json
     */
    public function addGroup(AddGroupRequest $request){
        return parent::api_addGroup($request);
    }

    /**
     * @param Request $request
     * @return string json
     */
    public function deleteGroup(Request $request){
        return parent::api_deleteGroup($request);
    }

    /**
     * @param Request $request
     * @return string json
     */
    public function addNewSettings(Request $request){
        return parent::api_addNewSettings($request);
    }

    /**
     * @param Request $request
     * @return string json
     */
    public function moveSetting(Request $request){
        return parent::api_moveSetting($request);
    }

    /**
     * @param DeleteSettingRequest $request
     * @return string json
     */
    public function deleteSetting(DeleteSettingRequest $request){
        return parent::api_deleteSetting($request);
    }

    /**
     * @return string json
     */
    public function getLanguages(){
        return parent::api_getLanguages();
    }

    /**
     * @param Request $request
     * @return string json
     */
    public function updateSettingsData(Request $request){
        return parent::api_updateSettingsData($request);
    }
}
