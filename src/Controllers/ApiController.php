<?php


namespace Finoghentov\NovaSettings\Controllers;

use App\Http\Controllers\Controller;
use Finoghentov\NovaSettings\Models\SettingConfig;
use Finoghentov\NovaSettings\Requests\ApiRequest;
use Finoghentov\NovaSettings\Models\Setting;
use Finoghentov\NovaSettings\Traits\DataSaving;
use Illuminate\Http\Request;


class ApiController extends Controller
{
    use DataSaving;

    protected $locales;

    public function __construct()
    {
        $this->locales = $this->getLocales();
    }

    /**
     * @return string json
     */
    protected function api_getLanguages(){
        return response()->json($this->locales);

    }

    /**
     * @return Setting json
     */
    protected function api_getSettings(){
        $settings = Setting::all();

        return response()->json($settings);
    }

    /**
     * Add new settings Group
     * @param ApiRequest $request
     * @return string json
     */
    protected function api_addGroup(ApiRequest $request){

        if(!Setting::checkUniqueGroupKey($request->group_title)){
            return response()->json('Duplicate group name ' . $request->group_title . ', please choose another one.', 422);
        }

        try{
            Setting::create($request->all());
        }catch(\Exception $e){
            return response()->json('Error while creating new group', 422);
        }
        return response()->json('success');
    }

    /**
     * Delete Group With Settings
     * @param Request $request
     * @return string json
     */
    protected function api_deleteGroup(Request $request){

        try{
            $group = Setting::find($request->id);
            $group->delete();
        }catch(\Exception $e){
            return response()->json('Error while deleting group', 422);
        }

        return response()->json('Group Deleted');
    }

    /**
     * Add new settings Group
     * @param ApiRequest $request
     * @return string json
     */
    protected function api_addNewSettings(Request $request){
        $settingsArr = [];

        $i=1;
        foreach($request->new_settings as $setting){

            $setting['key'] = Setting::keyFormat($setting['key']);

            if(isset($settingsArr[$setting['group']][$setting['key']])){
                return response()->json('Duplicate key ' . $setting['key'] . ', please choose another one.', 422);
            }
            $settingsArr[$setting['group']][$setting['key']] = [
                'order' => Setting::getNewOrder($setting['group'], $i),
                'name' => $setting['name'],
                'type' => $setting['type'],
                'value' => ''
            ];

            if(!$uniqueKeys = Setting::checkUniqueKey($setting['key'], $setting['group'])){
                return response()->json('Key ' . $setting['key'] . ' already using, please choose another one', 422);
            }
            $i++;
        }

        $changedSettingsGroups = Setting::whereIn('id',array_keys($settingsArr))->get();

        foreach($changedSettingsGroups as $item){
            $groupData = $item->getSettingsData();

            if(!$groupData){
                $item->settings_data = json_encode($settingsArr[$item->id], JSON_UNESCAPED_UNICODE);
            }else{
                foreach($settingsArr[$item->id] as  $key => $value){
                    $groupData[$key] = $value;
                }
                $item->settings_data = json_encode($groupData, JSON_UNESCAPED_UNICODE);
            }

            $item->save();
        }

        return response()->json('success');
    }

    /**
     * Drag Setting Item
     * @param Request $request
     * @return string json
     */
    protected function api_moveSetting(Request $request){
        $group = Setting::findOrFail($request->groupId);

        try{
            $data = $group->getSettingsData();

            $movingElement = $data[$request->key];
            $movingElement['order'] = $request->newIndex + 1;
            unset($data[$request->key]);

            foreach($data as $key => $value){
                if($value['order'] <= $request->newIndex + 1 && $value['order'] >= $request->oldIndex + 1){
                    $data[$key]['order']--;
                }
                if($value['order'] >= $request->newIndex + 1 && $value['order'] <= $request->oldIndex + 1){
                    $data[$key]['order']++;
                }
            }
            $data[$request->key] = $movingElement;

            $group->settings_data = json_encode($data,JSON_UNESCAPED_UNICODE);

            $group->save();
        }catch(\Exception $e){
            return response()->json('Error with moving field');
        }


        return response()->json('Field Moved');
    }

    /**
     * Delete Setting Item
     * @param ApiRequest $request
     * @return string json
     */
    protected function api_deleteSetting(ApiRequest $request){
        try{
            $setting = Setting::find($request->group_id);
        }catch(\Exception $e){
            return response()->json('Group not exists', 422);
        }
        $settings_data = $setting->getSettingsData();

        if(!isset($settings_data[$request->key])){
            return response()->json('Setting not exists', 422);
        }
        $order = $settings_data[$request->key]['order'];

        unset($settings_data[$request->key]);

        try{
            $setting->settings_data = json_encode($settings_data, JSON_UNESCAPED_UNICODE);
            $setting->save();
            Setting::sortAfterDelete($setting->id, $order);
        }catch (\Exception $e){
            return response()->json('Error with deleting setting', 422);
        }

        return response()->json('Setting Deleted');
    }

    /**
     * Update Settings Data
     * @param Request $request
     * @return string json
     */
    protected function api_updateSettingsData(Request $request){
        foreach ($request->field as $groupId => $data) {
            $group = Setting::find($groupId);

            $settingsData = $group->getSettingsData();

            foreach ($data as $setting_key => $value) {
                $settingItem = $settingsData[$setting_key];
                $settingsData[$setting_key]['value'] = $this->saveSetting($groupId ,$setting_key, $settingItem, $request);

            }

            $group->settings_data = json_encode($settingsData, JSON_UNESCAPED_UNICODE);
            $group->save();
        }
//        foreach(Setting::all() as $group){
//            $groupId = $group->id;
//            $settingsData = $group->getSettingsData();
//            foreach($settingsData as $key=>$value){
//                dd($value);
//                $settingsData[$key]['value'] = $this->saveSetting($groupId ,$key, $value, $request);
//
//            }
//
//            $group->settings_data = json_encode($settingsData, JSON_UNESCAPED_UNICODE);
//            $group->save();
//        }

        return redirect()->back();
    }


    /**
     * Return array of all settings languages or fallback language
     * @return array
     */
    private function getLocales(){
        if(!$langArr = config('app.settings_languages')){
            $langArr[] = config('app.fallback_locale');
        }

        return $langArr;
    }

    /**
     * Return all config at json format
     * @return SettingConfig json
     */
    protected function api_getConfig(){
        return response()->json(SettingConfig::all());
    }

    /**
     * Upload config to database
     */
    protected function api_uploadConfig(){
        try{
            SettingConfig::seed();
            return redirect()->back();
        }catch (\Exception $e){
            throw new \Exception('Error with uploading config');
        }
    }

    /**
     * Save settings config
     * @param Request $request
     * @return string
     */
    protected function api_saveConfig(Request $request){

        foreach(SettingConfig::all() as $item) {
            $item->value = $request->input($item->key) ?? 0;
            $item->save();
        }

        return redirect()->back();
    }

    /**
     * Getting settings config data
     *
     * @return mixed
     */
    protected function api_getConfigData(){
        return response()->json(SettingConfig::select('key', 'value')->get());
    }

    /**
     * Getting Editor Value
     * @return string
     */
    protected function api_getEditor(){
        return SettingConfig::where('key', 'config_editor')->firstOrFail()->value;
    }
}
